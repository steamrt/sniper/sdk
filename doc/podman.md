# Building in the Steam Runtime environment using Podman

[Podman](https://podman.io/) is a container tool broadly compatible
with Docker, but does not require root privileges or a daemon.
It is available in recent versions of most Linux distributions,
usually as a package named `podman`.
Please see its
[official documentation](https://github.com/containers/podman/blob/main/docs/tutorials/rootless_tutorial.md)
for more details of how to set it up in a "rootless" configuration.

The container image for the Steam Runtime SDK can be downloaded using Podman:

    podman pull registry.gitlab.steamos.cloud/steamrt/sniper/sdk

This container image can be used to compile games in a predictable,
consistent environment, with a relatively complete set of tools and
libraries included.

To use Podman for development, it will usually be necessary to share
parts of the host system's filesystem with the container.
Here is an example command-line to get an interactive shell in a container,
sharing your home directory with the container:

    podman run \
    --rm \
    --init \
    -v /home:/home \
    -v /etc/passwd:/etc/passwd:ro \
    -v /etc/group:/etc/group:ro \
    -e HOME="$HOME" \
    -h "$(hostname)" \
    -v /tmp:/tmp \
    -it \
    registry.gitlab.steamos.cloud/steamrt/sniper/sdk:latest \
    /bin/bash

For a more convenient development environment, consider using
[Toolbx](toolbx.md), which integrates well with Podman.

## Beta SDK

See the [README](../README.md) for more information about the beta SDK.

    podman pull registry.gitlab.steamos.cloud/steamrt/sniper/sdk:beta

## Advanced: Building your own

If the OCI registry on `registry.gitlab.steamos.cloud` is not available
to you for some reason, it is also possible to import the Steam Runtime
files as a new, locally-built OCI image.
This will require the `podman` and `buildah` tools.

To do this, you will need to download
`com.valvesoftware.SteamRuntime.Sdk-amd64,i386-sniper-sysroot.tar.gz` and
`com.valvesoftware.SteamRuntime.Sdk-amd64,i386-sniper-sysroot.Dockerfile`
from one of the versioned subdirectories of
<https://repo.steampowered.com/steamrt-images-sniper/snapshots/>.

Put the `-sysroot.tar.gz` and `-sysroot.Dockerfile` files
in an otherwise empty directory, `cd` into that directory, and import
them into Docker with a command like:

    podman build \
    -f com.valvesoftware.SteamRuntime.Sdk-amd64,i386-sniper-sysroot.Dockerfile \
    -t steamrt_sniper_amd64:latest \
    .

Then use `steamrt_sniper_amd64:latest` as the name of the image for your
container.
