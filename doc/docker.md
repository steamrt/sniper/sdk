# Building in the Steam Runtime environment using Docker

Docker is a widely-used container framework.
Please see
[official Docker documentation](https://docs.docker.com/get-started/overview/)
for more information about Docker.
It is available in recent versions of most Linux distributions,
usually as a package named `docker`, `docker-ce` or `docker.io`.

The container image for the Steam Runtime SDK can be downloaded using Docker:

    sudo docker pull registry.gitlab.steamos.cloud/steamrt/sniper/sdk

This container image can be used to compile games in a predictable,
consistent environment, with a relatively complete set of tools and
libraries included.

To use Docker for development, it will usually be necessary to share
parts of the host system's filesystem with the container.
Here is an example command-line to get an interactive shell in a container,
sharing your home directory with the container:

    sudo docker run \
    --rm \
    --init \
    -v /home:/home \
    -v /etc/passwd:/etc/passwd:ro \
    -v /etc/group:/etc/group:ro \
    -e HOME="$HOME" \
    -u "$(id -u):$(id -g)" \
    -h "$(hostname)" \
    -v /tmp:/tmp \
    -it \
    registry.gitlab.steamos.cloud/steamrt/sniper/sdk:latest \
    /dev/init -sg -- /bin/bash

For a more convenient development environment, consider using
[Toolbx](toolbx.md).

## Beta SDK

See the [README](../README.md) for more information about the beta SDK.

    sudo docker pull registry.gitlab.steamos.cloud/steamrt/sniper/sdk:beta

## Advanced: Building your own

If the Docker registry on `registry.gitlab.steamos.cloud` is not available
to you for some reason, it is also possible to import the Steam Runtime
files as a new, locally-built Docker image.

To do this, you will need to download
`com.valvesoftware.SteamRuntime.Sdk-amd64,i386-sniper-sysroot.tar.gz` and
`com.valvesoftware.SteamRuntime.Sdk-amd64,i386-sniper-sysroot.Dockerfile`
from one of the versioned subdirectories of
<https://repo.steampowered.com/steamrt-images-sniper/snapshots/>.

Put the `-sysroot.tar.gz` and `-sysroot.Dockerfile` files
in an otherwise empty directory, `cd` into that directory, and import
them into Docker with a command like:

    sudo docker build \
    -f com.valvesoftware.SteamRuntime.Sdk-amd64,i386-sniper-sysroot.Dockerfile \
    -t steamrt_sniper_amd64:latest \
    .

Then use `steamrt_sniper_amd64:latest` as the name of the image for your
container.
