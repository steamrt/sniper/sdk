# Building in the Steam Runtime environment using Toolbx

[Toolbx](https://containertoolbx.org/) is a tool to set up convenient
containerized command-line environments.
It uses the same storage and formats as [Podman](podman.md), but
provides more convenient sharing between the container and the
host operating system.
It is available in recent versions of most Linux distributions,
usually as a package named `toolbox` or `podman-toolbox`.
Recent versions of the Steam Runtime are available in a Toolbx-compatible
format, which make this a very convenient way to compile games and tools
that are intended to run in the Steam Runtime environment.

These container images can be used to compile games in a predictable,
consistent environment, with a relatively complete set of tools and
libraries included.

To download the container image and set up a Toolbx container,
use a command like this:

    toolbox create -i registry.gitlab.steamos.cloud/steamrt/sniper/sdk sniper

To enter the container environment, use a command like:

    toolbox enter sniper

You can download and install new packages using `sudo apt-get`,
the same as you would in an ordinary Debian or Ubuntu system.
Because Toolbx is based on unprivileged Podman containers, your normal
user is automatically given passwordless `sudo` access inside
the container, even if you do not have `sudo` access *outside* the
container.

## Not a security boundary

Please note that Toolbx is not designed to put a security boundary
between programs in the container and programs on the host.
Anything that you run inside the container has full access to your home
directory and privileges on the host system.
As a result, it is not safe to run potentially-malicious code inside
the Toolbx container.
If isolation between the container and the host is required,
use [Podman](podman.md) or [Docker](docker.md) instead.

## Beta SDK

To use the beta SDK, simply specify the `beta` tag:

    toolbox create -i registry.gitlab.steamos.cloud/steamrt/sniper/sdk:beta sniper

Games compiled in the beta SDK will not necessarily work with the
general-availability version of the container runtime.
See the [README](../README.md) for more information about the beta SDK.

## Mixing Toolbx with Podman

Toolbx uses the same storage and formats as [Podman](podman.md), so
it is easy to combine the two tools, for example using Toolbx for
interactive builds and debugging, then switching to Podman for a
final "clean" build that is better-isolated from the host system.

In particular, if your network connection is slow or unreliable, you
might find it useful to pre-download the container image using Podman,
which has better progress reporting:

    podman pull registry.gitlab.steamos.cloud/steamrt/sniper/sdk
