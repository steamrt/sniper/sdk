# Building in the Steam Runtime environment using schroot

We recommend using [Docker](docker.md), [Podman](podman.md) or
[Toolbx](toolbx.md) to build games for the Steam Runtime in a
predictable container environment.

However, some developers are more familiar with Debian's schroot tool,
and this can still be used (it is likely to work best on Debian or
Ubuntu machines).
This sets up a [chroot](https://en.wikipedia.org/wiki/Chroot) environment
containing Steam Runtime packages.

The
[steam-runtime Github repository](https://github.com/ValveSoftware/steam-runtime)
contains a script `setup_chroot.sh` which will create a Steam Runtime
chroot on your machine.
You will need the `schroot` tool installed, as well as root access through
`sudo`.
This chroot environment contains the same development libraries and tools
as the Docker container.

You should usually choose a build environment whose version
matches the Steam Runtime bundled with the current general-availability
(non-beta) Steam release.
For convenience,
<https://repo.steampowered.com/steamrt-images-sniper/snapshots/latest-container-runtime-depot/>
always points to that version.

For a different version, such as a beta or a "pinned"
older version, the version-numbered directories in
<https://repo.steampowered.com/steamrt-images-sniper/snapshots/>
correspond to the `version.txt` found in official Steam Runtime builds,
typically `~/.steam/root/ubuntu12_32/steam-runtime/version.txt` in a
Steam installation.

Download
`com.valvesoftware.SteamRuntime.Sdk-amd64,i386-sniper-sysroot.tar.gz`
from whichever version of the runtime is appropriate, then use a command
like:

    ./setup_chroot.sh --amd64 \
    --tarball ~/Downloads/com.valvesoftware.SteamRuntime.Sdk-amd64,i386-sniper-sysroot.tar.gz

This will create a chroot environment named `steamrt_sniper_amd64`.

Once setup-chroot.sh completes, you can use the **schroot** command to
execute any build operations within the Steam Runtime environment,
for example:

    ~/src/mygame$ schroot --chroot steamrt_sniper_amd64 -- make -f mygame.mak

The chroot should be set up so that the path containing the build tree is
the same inside as outside the root.

If this path is not within `/home`, it should be added to
`/etc/schroot/default/fstab`.
For example, if you keep source and build trees in `/srv/mygame`, you
could add `/srv` or `/srv/mygame` to `/etc/schroot/default/fstab`.
Then the next time the root is entered, this path will be available inside the root.

The setup script can be re-run to re-create the schroot environment.
For example, you can do this to reset the installed packages to a
"clean slate", or to upgrade to a new `-sysroot.tar.gz` file.
