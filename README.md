# sniper SDK

This project provides container images for the Steam Runtime 3 'sniper'
SDK.

For general information about sniper, please see the README in the
`steamrt/sniper` branch of the `steamrt` package:
<https://gitlab.steamos.cloud/steamrt/steamrt/-/blob/steamrt/sniper/README.md>.

## Developing software that runs in sniper

A [guide to the Steam Linux Runtime for game developers][slr-for-game-developers]
is available.

Native Linux games that require sniper can be released on Steam.
Since October 2024, this is available as a "self-service"
feature via the Steamworks partner web interface, which can be used by
any game that benefits from a newer library stack.
To use this feature, your app must first set up a Launch Option that
supports Linux.
Once that is set up, you can use the Installation → Linux Runtime
menu item to select a runtime.

Early adopters for this mechanism include
[Battle for Wesnoth][Wesnoth on sniper],
[Endless Sky][Endless Sky on sniper] and
[Retroarch][Retroarch on sniper].

We recommend compiling in an OCI-based container using a framework such
as Docker or Podman.
This ensures that only the libraries in the container are used for
compilation, as well as making builds more predictable and reproducible.
See [below](#oci) for more details.

Previous versions of the Steam Runtime, published before container
frameworks had become widespread, recommended using a
[chroot](https://en.wikipedia.org/wiki/Chroot)
environment via the `schroot` tool.
We no longer recommend this, because it is more difficult to set up on
non-Debian-derived machines and requires root privileges, but it is
still possible: see [below](#tar).

## Testing software that runs in sniper

The primary way to test software that runs in sniper is to install it
via Steam.
For games that have not yet been published on Steam, sniper can be downloaded
with a command like `steam steam://install/1628350`,
and the game can be added to Steam as a shortcut, either by using Steam's
[non-Steam game][] feature or by using the [devkit tool][].

There is also a [Platform OCI image][] which can be used to run software
that was compiled for sniper in a CI environment.

## OCI images for Docker and Podman <a name="oci"></a>

For Docker ([more information](doc/docker.md)):

    sudo docker pull registry.gitlab.steamos.cloud/steamrt/sniper/sdk

or for [Podman](https://podman.io/) ([more information](doc/podman.md)):

    podman pull registry.gitlab.steamos.cloud/steamrt/sniper/sdk

or for [Toolbx](https://containertoolbx.org/)
([more information](doc/toolbx.md)):

    toolbox create -i registry.gitlab.steamos.cloud/steamrt/sniper/sdk sniper
    toolbox enter sniper

or for [Distrobox](https://distrobox.it/)
([more information](doc/distrobox.md)):

    distrobox create -i registry.gitlab.steamos.cloud/steamrt/sniper/sdk sniper
    distrobox enter sniper

`registry.gitlab.steamos.cloud/steamrt/sniper/sdk` is the 64-bit SDK.
Most libraries also have development files available for 32-bit builds.
The development files for a few libraries that use legacy `-config` scripts
(`libfltk1.1-dev`, `libgcrypt20-dev` and `libgpg-error-dev`) are currently
64-bit-only.

There is no separate 32-bit SDK for sniper, and building games as 64-bit
is strongly recommended.

Tags available for this repository (can be specified after `:` when using
`docker pull`):

* `3.0.20250108.112707` (etc.): Specific versions of the SDK. Use one of these
    if you need to "pin" to an older version for reproducible builds.
    These version numbers correspond exactly to
    <https://repo.steampowered.com/steamrt3/images/3.0.20250108.112707/>
    and so on.

* `latest`, `latest-container-runtime-depot`:
    An alias pointing to the SDK corresponding to the runtime that is included
    in the current public stable version of the SteamLinuxRuntime_sniper depot.
    If in doubt, build games against this.
    This should always match the version in
    <https://repo.steampowered.com/steamrt3/images/latest-container-runtime-depot/>.

* `beta`, `latest-container-runtime-public-beta`:
    An alias pointing to the SDK corresponding to the runtime that is included
    in the most recent public beta version of the SteamLinuxRuntime_sniper
    depot. This should always match the version in
    <https://repo.steampowered.com/steamrt3/images/latest-container-runtime-public-beta/>.
    If there is currently no public beta available, then this version will
    usually be the same as `latest`, or it might even be older than `latest`.
    Only use this if you know that it is what you want.

These OCI images were prepared using Docker, but should be equally suitable
for other OCI-compatible tools like `podman`.

## Tar archives for schroot and other non-OCI environments <a name="tar"></a>

We recommend Docker as the preferred way to develop against sniper.
However, for those who are more familiar with the `schroot` tool or other
non-OCI-based chroot and container environments, the `-sysroot.tar.gz`
archives available from
<https://repo.steampowered.com/steamrt3/images/>
contain the same packages as the official OCI images.
[More information about schroot](doc/schroot.md)

If you prefer other chroot or container frameworks such as
`systemd-nspawn`, the same tar archives can be unpacked into a directory
and used with those.

The versions available are the same as for the [OCI images](#oci).

## Toolchains

Several sets of compilers are available:

* gcc-9 and g++-9
* gcc-10 and g++-10 (default)
* gcc-12 and g++-12 (experimental)
* gcc-14 and g++-14 (experimental)
* clang-11 and clang++-11

The experimental `gcc-12` and `g++-12` can be installed by using
`apt-get install gcc-12-monolithic` but are not currently included in
the SDK itself.

Similarly,
the experimental `gcc-14` and `g++-14` can be installed by using
`apt-get install gcc-14-monolithic`.
At the time of writing,
this requires adding the [beta apt source](#apt-packages).
Starting from release 3.0.20250210.116596,
`gcc-14` will be installed by default.

To avoid an unexpected upgrade of the Standard C++ library,
`gcc-12`, `g++-12` and any newer versions always behave as though the
`-static-libgcc` and `static-libstdc++` options had been used.

All C++ code in your project should usually be built with the same compiler.
C-based external dependencies such as SDL and GLib are safe to use, but
passing a STL object such as `std::string` to C++-based libraries from
the Steam Runtime or the operating system (for example `libgnutlsxx` or
`libpcrecpp`) will not necessarily work and is best avoided, especially
if you are using `g++-12` or newer.

Most build systems have a way to use a non-default compiler by specifying
its name, for example Autotools `./configure CC=clang CXX=clang++ ...` and
CMake `cmake -DCMAKE_C_COMPILER=clang -DCMAKE_CXX_COMPILER=clang++ ...`.

Meson users can use a command like `meson setup --native-file=clang.txt ...`
to select a different compiler.
See `/usr/share/meson/native` in the container for all the options available.

Starting from release 3.0.20250210.116596,
CMake users can use a command like
`cmake --toolchain=/usr/share/steamrt/cmake/gcc-14.txt`
to select a different compiler.
See `/usr/share/steamrt/cmake` in the container for all the options available.

Another mechanism that can select a different compiler is that after
installing `gcc-14-monolithic` and
setting environment variable `PATH="/usr/lib/gcc-14/bin:$PATH"`,
the `gcc`, `g++`, `x86_64-linux-gnu-gcc` and `x86_64-linux-gnu-g++`
commands will run gcc 14.

### mold linker

By default,
the standard `ld.bfd` from GNU binutils 2.35.x is used for linking,
matching Debian 11.

An experimental backport of the `mold` linker from Debian 13 is available
for installation via `apt-get install mold` and can be used with
command-lines like `g++ -B/usr/libexec/mold` or `g++-12 -fuse-ld=mold`.
The `-fuse-ld=mold` command-line option only works with gcc 12 or later.

Meson users can use a command like
`meson setup --native-file=gcc-14.txt --native-file=mold.txt ...`
to select the mold linker.

## 32-bit code

The sniper SDK can compile 32-bit code in two different ways:

* cross-compiler-style, by using prefixed tools such as
    `i686-linux-gnu-gcc` and `i686-linux-gnu-objcopy`;
* multilib-style, by passing `-m32` to the default 64-bit compiler

Both should result in binaries that link to the same libraries.

For gcc versions newer than the default,
before release 3.0.20250210.116596,
only the multilib style (`-m32`) was available.

When building 32-bit binaries,
it is important to discover dependency libraries via the 32-bit
pkg-config wrapper,
`i686-linux-gnu-pkg-config`.
For Autotools users,
after configuring with
`./configure --build=x86_64-linux-gnu --host=i686-linux-gnu`,
standard macros such as `PKG_PROG_PKG_CONFIG` and `PKG_CHECK_MODULES`
should automatically select `i686-linux-gnu-pkg-config`.
For Meson and CMake users,
all of the 32-bit cross-file and toolchain files described below will
also select `i686-linux-gnu-pkg-config`.
In other build systems,
setting environment variable `PKG_CONFIG=i686-linux-gnu-pkg-config`
before compiling 32-bit code might be helpful.

Meson users can use a command like `meson setup --cross-file=gcc-m32.txt ...`
or `meson setup --cross-file=i686-linux-gnu-gcc.txt ...`
to select a multilib toolchain or cross-compiler.
This will also use `i686-linux-gnu-pkg-config` to find dependencies.
See `/usr/share/meson/cross` in the container for all the options available.

Starting from release 3.0.20250210.116596,
CMake users can use a command like
`cmake --toolchain=/usr/share/steamrt/cmake/gcc-14-m32.txt`
to select a different multilib toolchain or cross-compiler.
The 32-bit toolchain files also use `i686-linux-gnu-pkg-config` to find
dependencies.
See `/usr/share/steamrt/cmake` in the container for all the options available.

Starting from release 3.0.20250210.116596,
after installing `gcc-14-monolithic` and
setting environment variable `PATH="/usr/lib/gcc-14/bin:$PATH"`,
the `i686-linux-gnu-gcc` and `i686-linux-gnu-g++` commands will run gcc 14
in `-m32` mode.

## apt packages

All of the packages that are supported by the Platform runtime are included
in the SDK image and do not need to be downloaded separately. An apt repository
is also available, and is preconfigured in the OCI images:

    # corresponding to the :latest OCI image
    deb https://repo.steampowered.com/steamrt3/apt soldier main contrib non-free
    deb-src https://repo.steampowered.com/steamrt3/apt soldier main contrib non-free
    # corresponding to the :beta OCI image and not configured by default
    #deb https://repo.steampowered.com/steamrt3/apt soldier_beta main contrib non-free
    #deb-src https://repo.steampowered.com/steamrt3/apt soldier_beta main contrib non-free

sniper is based on, and broadly compatible with, Debian 11 'bullseye'.
Many packages in the SDK are taken from Debian 11 without modification,
and those packages are not included in the sniper-specific apt repository.

If additional development tools are required, you can install them
from the Debian 11 'bullseye' apt repositories. However, please be careful
not to introduce dependencies on packages that are not included in the
sniper Platform. The sniper Platform is the only thing guaranteed to be
available at runtime.

Additional development tools can also be installed from bullseye-backports,
from third-party apt repositories compatible with Debian 11, or from
source code. Again, please be careful not to introduce dependencies on
packages that are not included in the sniper Platform.

## Source code

Source code for all the packages that go into the OCI image can be found
in the appropriate subdirectory of
<https://repo.steampowered.com/steamrt3/images/>
(look in the `sources/` directory).

The OCI image is built using
[flatdeb-steam](https://gitlab.steamos.cloud/steamrt/flatdeb-steam).
The choice of packages to include in the runtime is partly in the
flatdeb configuration, but mostly controlled by
[the steamrt/sniper branch of the steamrt source package](https://gitlab.steamos.cloud/steamrt/steamrt/-/tree/steamrt/sniper).

Another relevant project is
[steam-runtime-tools](https://gitlab.steamos.cloud/steamrt/steam-runtime-tools),
which includes the `pressure-vessel` container-runner tool, the
`steam-runtime-system-info` diagnostic tool, and some library code that
they share, and also builds the Steampipe depot used to integrate this
runtime into the SteamPlay compatibility tool mechanism.

[Endless Sky on sniper]: https://github.com/ValveSoftware/steam-runtime/issues/556
[Platform OCI image]: https://gitlab.steamos.cloud/steamrt/sniper/platform
[Retroarch on sniper]: https://github.com/libretro/RetroArch/issues/14266
[Wesnoth on sniper]: https://github.com/ValveSoftware/steam-runtime/issues/508#issuecomment-1147665747
[devkit tool]: https://partner.steamgames.com/doc/steamdeck/loadgames
[non-Steam game]: https://help.steampowered.com/en/faqs/view/4B8B-9697-2338-40EC
[slr-for-game-developers]: https://gitlab.steamos.cloud/steamrt/steam-runtime-tools/-/blob/main/docs/slr-for-game-developers.md
